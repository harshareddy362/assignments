=begin
Create a project using ruby version 2.2.0, write a method which takes array 
as an input and returns an array in the order of smallest, largest, 2nd smallest, 2nd largest .. 
Print both the input and output arrays.
Ex: [1,2,4,6,3,5] sol: [1,6,2,5,3,4]
=end

#solution
def sort_arr_ls(arr)
	arr.sort!
	s = 0
	e = arr.length-1
	a = Array.new
	while s < e
		a << arr[s]
		a << arr[e]
		s = s+1
		e = e-1
	end
	if s == e
		a << arr[s]
	end
	return a
end

n = gets.chomp.to_i
ar = Array.new
for i in (1..n) do
	x = gets.chomp.to_i
	ar << x
end
print sort_arr_ls ar
