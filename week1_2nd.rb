=begin
Create a project using ruby version 2.3.0, write a method to generate array of squares using proc/lambda
Ex: 5 sol: [25, 16, 9, 4, 1]
=end

#solution

x = lambda do |a|
	arr = Array.new
	while a>0
		arr.append(a**2)
		a = a-1
	end
	return arr
end
n = gets.chomp.to_i
print(x.call(n)) 